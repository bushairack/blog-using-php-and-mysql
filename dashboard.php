<?php
#***************************************************************************************#

				#****************************************#
				#********** PAGE CONFIGURATION **********#
				#****************************************#


				require_once('./include/config.inc.php');
				require_once('./include/form.inc.php');
				require_once('./include/db.inc.php');


#***************************************************************************************#


				#****************************************#
				#********** SECURE PAGE ACCESS **********#
				#****************************************#
				
				require_once('include/pageAccess.inc.php');
				

#***************************************************************************************#


				#***************************************#
				#******** INITIALIZE VARIABLES *********#
				#***************************************#

				$errorImageUpload			= NULL;
				$errorAccInfo 				= NULL;
				$blogHeading 				= NULL;
				$blogContent 				= NULL;
				$blogImagePath 			= NULL;
				$errorblogHeading 		= NULL;
				$errorblogContent  		= NULL;
				$catlabelErrormessage  	= NULL;
				$dbSuccess  				= NULL;
				$dbError  					= NULL;
				
#***************************************************************************************#


				#******************************************************#
				#********** FETCH PROFILE DATA FROM DATABASE **********#
				#******************************************************#
				
if(DEBUG)	echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Lese Profildaten aus DB aus... <i>(" . basename(__FILE__) . ")</i></p>\n";
				
				// Schritt 1 DB: DB-Verbindung herstellen
				$PDO = dbConnect('blog');
				
						$sql 		= 'SELECT * FROM users
										WHERE userID = :ph_userID';
						
						$params 	= array( 'ph_userID' => $userID );
				
			
				// Schritt 2 DB: SQL-Statement vorbereiten
				$PDOStatement = $PDO->prepare($sql);
				
				// Schritt 3 DB: SQL-Statement ausführen und ggf. Platzhalter füllen
				try {	
					$PDOStatement->execute($params);						
				} catch(PDOException $error) {
if(DEBUG)		echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: FEHLER: " . $error->GetMessage() . "<i>(" . basename(__FILE__) . ")</i></p>\n";										
					$dbError = 'Fehler beim Zugriff auf die Datenbank!';
				}
				
				$row = $PDOStatement->fetch(PDO::FETCH_ASSOC);
				
if(DEBUG_V)	echo "<pre class='debug value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)	print_r($row);					
if(DEBUG_V)	echo "</pre>";

				// DB-Verbindung beenden
				unset($PDO);
				$userFirstName			= $row['userFirstName'];
				$userLastName			= $row['userLastName'];
				

#***************************************************************************************#
				


								// Schritt 1 DB: DB-Verbindung herstellen
								$PDO = dbConnect('blog');
								
								$sql 		= 'SELECT * FROM categories';
										
								$params	=	array();


								$PDOStatement = $PDO->prepare($sql);
								
								// Schritt 3 DB: SQL-Statement ausführen und ggf. Platzhalter füllen
								try {	
									$PDOStatement->execute($params);						
								} catch(PDOException $error) {
if(DEBUG)						echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: FEHLER: " . $error->GetMessage() . "<i>(" . basename(__FILE__) . ")</i></p>\n";										
									$dbError = 'Fehler beim Zugriff auf die Datenbank!';
								}
								
								
							$categoriesArray	= $PDOStatement->fetchAll(PDO::FETCH_ASSOC);
							
if(DEBUG_V)	echo "<pre class='debug value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)	print_r($categoriesArray);					
if(DEBUG_V)	echo "</pre>";



#***************************************************************************************#


				#********************************************#
				#********** PROCESS URL PARAMETERS **********#
				#********************************************#
				
				#********** PREVIEW GET ARRAY **********#
/*
if(DEBUG_V)	echo "<pre class='debug value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)	print_r($_GET);					
if(DEBUG_V)	echo "</pre>";
*/
				#****************************************#
				
				// Schritt 1 URL: Prüfen, ob URL-Parameter übergeben wurde
				if( isset($_GET['action']) ) {
if(DEBUG)		echo "<p class='debug'>🧻 <b>Line " . __LINE__ . "</b>: URL-Parameter 'action' wurde übergeben. <i>(" . basename(__FILE__) . ")</i></p>\n";										
					
					// Schritt 2 URL: Werte auslesen, entschärfen, DEBUG-Ausgabe
					$action = cleanString($_GET['action']);
if(DEBUG_V)		echo "<p class='debug value'><b>Line " . __LINE__ . "</b>: \$action: $action <i>(" . basename(__FILE__) . ")</i></p>\n";
					
					// Schritt 3 URL: Verzweigung
					
					
					#********** LOGOUT **********#
					if( $action === 'logout' ) {
if(DEBUG)			echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Logout wird durchgeführt... <i>(" . basename(__FILE__) . ")</i></p>\n";
						// Schritt 4 URL: Daten weiterverarbeiten
						session_regenerate_id(true);
						
						// 1. Bestehende Session löschen
						session_destroy();
						// 2. User auf Index-Seite umleiten
						header('LOCATION: index.php');
						
						exit;	
					}
					 
					if( $action === 'zumIndex' ) {
						//session_regenerate_id(true);
						//$showDashBoard	= true;
						header('LOCATION: index.php');

					}
				}
				
#***************************************************************************************#	


				
				
				#***********************************************#
				#********** PROCESS FORM NEW Blog **************#
				#***********************************************#


if(DEBUG_V)	echo "<pre class='debug value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)	print_r($_POST);					
if(DEBUG_V)	echo "</pre>";


				// Schritt 1 FORM: Prüfen, ob Formular abgeschickt wurde
				if( isset($_POST['newBlog']) ) {
if(DEBUG)		echo "<p class='debug'>🧻 <b>Line " . __LINE__ . "</b>: Formular 'New Blog' wurde abgeschickt. <i>(" . basename(__FILE__) . ")</i></p>\n";										

					// Schritt 2: Werte auslesen, entschärfen, DEBUG-Ausgabe
if(DEBUG)		echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Werte auslesen und entschärfen... <i>(" . basename(__FILE__) . ")</i></p>\n";

				}

#*************************************************************************************#

				#***********************************************#
				#***************** EDIT CATEGORY ***************#
				#***********************************************#

				#********** PREVIEW POST ARRAY **********#



				#****************************************#				
				// Schritt 1 FORM: Prüfen, ob Formular abgeschickt wurde
				if( isset($_POST['blogCategory']) ) {
if(DEBUG)		echo "<p class='debug'>🧻 <b>Line " . __LINE__ . "</b>: Formular 'Edit Profile' wurde abgeschickt. <i>(" . basename(__FILE__) . ")</i></p>\n";										
					
					// Schritt 2 FORM: Werte auslesen, entschärfen, DEBUG-Ausgabe
if(DEBUG)		echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Werte werden ausgelesen und entschärft... <i>(" . basename(__FILE__) . ")</i></p>\n";
					
					$catLabel 	= cleanString( $_POST['catLabel'] );	
						
					
if(DEBUG_V)		echo "<p class='debug value'><b>Line " . __LINE__ . "</b>: \$catLabel: $catLabel <i>(" . basename(__FILE__) . ")</i></p>\n";

					if($catLabel !== NULL) {
					#***********************************#
					#********** DB OPERATIONS **********#
					#***********************************#
					
					//$blogID = 1;
					
					#********** CHECK IF CATEGORY IS ALREADY PRESENT **********#
if(DEBUG)		echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Prüfen, ob Category bereits ... <i>(" . basename(__FILE__) . ")</i></p>\n";


							// Schritt 1 DB: DB-Verbindung herstellen
							$PDO = dbConnect('blog');
							
							$sql 		= 'SELECT * FROM categories
											WHERE catLabel = :ph_catLabel';
											
							$params 	= array('ph_catLabel' => $catLabel);
													
							// Schritt 2 DB: SQL-Statement vorbereiten
							$PDOStatement = $PDO->prepare($sql);
							
						
							
							// Schritt 3 DB: SQL-Statement ausführen und ggf. Platzhalter füllen
							try {	
								$PDOStatement->execute($params);						
							} catch(PDOException $error) {
if(DEBUG)					echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: FEHLER: " . $error->GetMessage() . "<i>(" . basename(__FILE__) . ")</i></p>\n";										
								$dbError = 'Fehler beim Zugriff auf die Datenbank!';
							}
							
							// Schritt 4 DB: Daten weiterverarbeiten
							$count = $PDOStatement->fetchColumn();
if(DEBUG_V)				echo "<p class='debug value'><b>Line " . __LINE__ . "</b>: \$count: $count <i>(" . basename(__FILE__) . ")</i></p>\n";
							

								if( $count > 0 ) {
									// Fehlerfall
if(DEBUG)						echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: category exist <i>(" . basename(__FILE__) . ")</i></p>\n";				
									$catLabel = NULL;	
									$catlabelErrormessage = 'category already exist';
								} else {
								// Erfolgsfall
if(DEBUG)					echo "<p class='debug ok'><b>Line " . __LINE__ . "</b>:  '$catLabel' category not exist. <i>(" . basename(__FILE__) . ")</i></p>\n";				
							

								
								#********** INSERT CATEGORY INTO DATABASE **********#
if(DEBUG)					echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Speichere Profildaten in die Datenbank... <i>(" . basename(__FILE__) . ")</i></p>\n";
						
								// Schritt 1 DB: DB-Verbindung herstellen
								$PDO = dbConnect('blog');
								
								$sql 		= 'INSERT INTO categories (catLabel)
												VALUES (:ph_catLabel)';
												
								$params	=	array(
														'ph_catLabel'		=> $catLabel
														);
					
							}
if(DEBUG_V)	echo "<pre class='debug value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)	print_r($params);					
if(DEBUG_V)	echo "</pre>";
						
								// Schritt 2 DB: SQL-Statement vorbereiten
								$PDOStatement = $PDO->prepare($sql);
								
								// Schritt 3 DB: SQL-Statement ausführen und ggf. Platzhalter füllen
								try {	
									$PDOStatement->execute($params);						
								} catch(PDOException $error) {
if(DEBUG)						echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: FEHLER: " . $error->GetMessage() . "<i>(" . basename(__FILE__) . ")</i></p>\n";										
									$dbError = 'Fehler beim Zugriff auf die Datenbank!';
								}
								
																							
							// Schritt 4 DB: Daten weiterverarbeiten
							// Bei schreibendem Zugriff: Schreiberfolg prüfen
							$rowCount = $PDOStatement->rowCount();
if(DEBUG_V)				echo "<p class='debug value'><b>Line " . __LINE__ . "</b>: \$rowCount: $rowCount <i>(" . basename(__FILE__) . ")</i></p>\n";
								
							if( !$rowCount ) {
								// Fehlerfall
if(DEBUG)					echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: FEHLER beim Speichern des neuen Mitarbeiters! <i>(" . basename(__FILE__) . ")</i></p>\n";				
								$dbError = 'Es ist ein Fehler aufgetreten! Bitte versuchen Sie es später noch einmal.';
								
							} else {
								// Erfolgsfall
								// Nach dem erfolgreichen Schreiben in die DB die letzte vergebene ID auslesen
								$newcatID = $PDO->lastInsertId();
if(DEBUG)					echo "<p class='debug ok'><b>Line " . __LINE__ . "</b>: Neuer category erfolgreich unter ID$newcatID in der DB gespeichert. <i>(" . basename(__FILE__) . ")</i></p>\n";				
									
							}
								
							// DB-Verbindung beenden
							//unset($PDO);
						}
				}
				
#***************************************************************************************#	

						#**********************************#
						#********** IMAGE UPLOAD **********#
						#**********************************#	
						
						// Schritt 1 FORM: Prüfen, ob Formular abgeschickt wurde
					if( isset($_POST['blogContent']) ) {
						
					
						

if(DEBUG_V)			echo "<pre class='debug value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)			print_r($_FILES);					
if(DEBUG_V)			echo "</pre>";

						#********** CHECK IF IMAGE UPLOAD IS ACTIVE **********#
						if( $_FILES['avatar']['tmp_name'] === '' ) {
							// image upload is not active
if(DEBUG)				echo "<p class='debug hint'><b>Line " . __LINE__ . "</b>: Bildupload ist NICHT aktiv. <i>(" . basename(__FILE__) . ")</i></p>\n";				
							
						} else {
							// image upload is active
if(DEBUG)				echo "<p class='debug hint'><b>Line " . __LINE__ . "</b>: Bildupload ist aktiv. <i>(" . basename(__FILE__) . ")</i></p>\n";				
							
					
						
						
							$imageUploadReturnArray = imageUpload( $_FILES['avatar']['tmp_name'] );
							
if(DEBUG_V)				echo "<pre class='debug value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)				print_r($imageUploadReturnArray);					
if(DEBUG_V)				echo "</pre>";
					
	


							#********** VALIDATE IMAGE UPLOAD **********#
							if( $imageUploadReturnArray['imageError'] !== NULL AND $imageUploadReturnArray['imagePath'] === NULL ) {
								// Fehlerfall
if(DEBUG)					echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: FEHLER beim Bildupload: <i>'$imageUploadReturnArray[imageError]'</i> <i>(" . basename(__FILE__) . ")</i></p>\n";				
								$errorImageUpload = $imageUploadReturnArray['imageError'];
								
							} else {
								// Erfolgsfall
if(DEBUG)					echo "<p class='debug ok'><b>Line " . __LINE__ . "</b>: Bild wurde erfolgreich unter <i>'$imageUploadReturnArray[imagePath]'</i> gespeichert. <i>(" . basename(__FILE__) . ")</i></p>\n";				
								

						
					// IMAGE UPLOAD END
						
						#*****************************************************#
					
						
						#********** FINAL IMAGE UPLOAD VALIDATION **********#
						if( $errorImageUpload !== NULL ) {
							// Fehlerfall
if(DEBUG)				echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: FINAL IMAGE UPLOAD VALIDATION: Das Formular enthält noch Fehler! <i>(" . basename(__FILE__) . ")</i></p>\n";				
							
						} else {
							// Erfolgsfall
if(DEBUG)				echo "<p class='debug ok'><b>Line " . __LINE__ . "</b>: FINAL IMAGE UPLOAD VALIDATION: Das Formular ist formal fehlerfrei. <i>(" . basename(__FILE__) . ")</i></p>\n";				
							
							
							// Schritt 4 FORM: Daten weiterverarbeiten
if(DEBUG)				echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Daten werden weiterverarbeitet... <i>(" . basename(__FILE__) . ")</i></p>\n";
						
						
							$blogImagePath		= $imageUploadReturnArray['imagePath'];
							
//if(DEBUG_V)		echo "<p class='debug value'><b>Line " . __LINE__ . "</b>: \$blogImagePath: $blogImagePath <i>(" . basename(__FILE__) . ")</i></p>\n";

						}
					}
				}





			
#***************************************************************************************#				

				#***********************************************#
				#******************** EDIT BLOG ****************#
				#***********************************************#






#***************************************************************************************#						
				// Schritt 1 FORM: Prüfen, ob Formular abgeschickt wurde
	//			if( isset($_POST['blogContent']) ) {

if(DEBUG)		echo "<p class='debug'>🧻 <b>Line " . __LINE__ . "</b>: Formular 'blogContent' wurde abgeschickt. <i>(" . basename(__FILE__) . ")</i></p>\n";										
					
					// Schritt 2 FORM: Werte auslesen, entschärfen, DEBUG-Ausgabe
if(DEBUG)		echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Werte werden ausgelesen und entschärft... <i>(" . basename(__FILE__) . ")</i></p>\n";
					


								// Schritt 1 DB: DB-Verbindung herstellen
								$PDO = dbConnect('blog');
								
								$sql 		= 'SELECT * FROM categories';
										
								$params	=	array();


								$PDOStatement = $PDO->prepare($sql);
								
								// Schritt 3 DB: SQL-Statement ausführen und ggf. Platzhalter füllen
								try {	
									$PDOStatement->execute($params);						
								} catch(PDOException $error) {
if(DEBUG)						echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: FEHLER: " . $error->GetMessage() . "<i>(" . basename(__FILE__) . ")</i></p>\n";										
									$dbError = 'Fehler beim Zugriff auf die Datenbank!';
								}
								
								
							$categoriesArray	= $PDOStatement->fetchAll(PDO::FETCH_ASSOC);
							
if(DEBUG_V)	echo "<pre class='debug value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)	print_r($categoriesArray);					
if(DEBUG_V)	echo "</pre>";


			
					$blogHeading 				= cleanString($_POST['heading']);
					$blogContent 				= cleanString($_POST['blogDatas']);
					$catID						= cleanString($_POST['catLabels']);
					$blogImageAlignment		= cleanString($_POST['blogImageAlignment']);
					//$blogImagePath			= cleanString($_POST['image']);
					

if(DEBUG_V)		echo "<p class='debug value'><b>Line " . __LINE__ . "</b>: \$blogHeading: $blogHeading <i>(" . basename(__FILE__) . ")</i></p>\n";
if(DEBUG_V)		echo "<p class='debug value'><b>Line " . __LINE__ . "</b>: \$blogContent: $blogContent <i>(" . basename(__FILE__) . ")</i></p>\n";
if(DEBUG_V)		echo "<p class='debug value'><b>Line " . __LINE__ . "</b>: \$blogImageAlignment: $blogImageAlignment <i>(" . basename(__FILE__) . ")</i></p>\n";
if(DEBUG_V)		echo "<p class='debug value'><b>Line " . __LINE__ . "</b>: \$catID: $catID <i>(" . basename(__FILE__) . ")</i></p>\n";


					// Schritt 3 FORM: Daten validieren
if(DEBUG)		echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Feldvalidierung... <i>(" . basename(__FILE__) . ")</i></p>\n";
					
					$errorblogHeading 		= checkInputString($blogHeading);
					$errorblogContent 		= checkInputString($blogContent);
					
/*					
if(DEBUG_V)		echo "<p class='debug value'><b>Line " . __LINE__ . "</b>: \$errorblogHeading: $errorblogHeading <i>(" . basename(__FILE__) . ")</i></p>\n";
if(DEBUG_V)		echo "<p class='debug value'><b>Line " . __LINE__ . "</b>: \$errorblogContent: $errorblogContent <i>(" . basename(__FILE__) . ")</i></p>\n";
*/
					
					if( $errorblogHeading OR $errorblogContent ) {
						// Fehlerfall
if(DEBUG)			echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: Das Formular enthält Fehler! <i>(" . basename(__FILE__) . ")</i></p>\n";				
					
					} else {
						// Erfolgsfall
if(DEBUG)			echo "<p class='debug ok'><b>Line " . __LINE__ . "</b>: Das Formular ist fehlerfrei und wird nun verarbeitet... <i>(" . basename(__FILE__) . ")</i></p>\n";				
						
						// Schritt 4 FORM: Daten weiterverarbeiten
						
						#***********************************#
						#********** DB OPERATIONS **********#
						#***********************************#
						
						// Schritt 1 DB: DB-Verbindung herstellen
						$PDO = dbConnect('blog');
						
						#********** 1. ADD NEW BLOG **********#				
						$sql 		= 'INSERT INTO blogs
										(userID, catID, blogHeadline, blogImagePath, blogImageAlignment, blogContent)
										VALUES
										(:ph_userID, :ph_catID, :ph_blogHeading, :ph_blogImagePath, :ph_blogImageAlignment, :ph_blogContent)
										';
									  
						$params 	= array(
													'ph_userID'						=> $userID,
													'ph_catID'						=> $catID,
													'ph_blogHeading' 				=> $blogHeading,
													'ph_blogImagePath'			=> $blogImagePath,
													'ph_blogImageAlignment'		=> $blogImageAlignment,
													'ph_blogContent' 				=> $blogContent
													
													);
							
							// Schritt 2 DB: SQL-Statement vorbereiten
							$PDOStatement = $PDO->prepare($sql);
						
						
if(DEBUG_V)	echo "<pre class='debug value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)	print_r($params);					
if(DEBUG_V)	echo "</pre>";	
						
							// Schritt 3 DB: SQL-Statement ausführen und ggf. Platzhalter füllen
							try {	
								$PDOStatement->execute($params);						
							} catch(PDOException $error) {
if(DEBUG)					echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: FEHLER: " . $error->GetMessage() . "<i>(" . basename(__FILE__) . ")</i></p>\n";										
								$dbError = 'Fehler beim Zugriff auf die Datenbank!';
							}
							
							// Schritt 4 DB: Daten weiterverarbeiten
							
							$rowCount = $PDOStatement->rowCount();
if(DEBUG_V)				echo "<p class='debug value'><b>Line " . __LINE__ . "</b>: \$rowCount: $rowCount <i>(" . basename(__FILE__) . ")</i></p>\n";
								
							if( !$rowCount ) {
								// Fehlerfall
if(DEBUG)					echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: FEHLER beim Speichern des neuen Blog! <i>(" . basename(__FILE__) . ")</i></p>\n";				
								$dbError = 'Es ist ein Fehler aufgetreten! Bitte versuchen Sie es später noch einmal.';
								
							} else {
								// Erfolgsfall
								$currentBlogId = $PDO->lastInsertId();
if(DEBUG)					echo "<p class='debug ok'><b>Line " . __LINE__ . "</b>: New blogID$currentBlogId in der DB gespeichert. <i>(" . basename(__FILE__) . ")</i></p>\n";				
								$dbSuccess = 'Your data successfully stored in to database.';
								
							}

						}
					}
				
				






#***************************************************************************************#
?>



<!doctype html>

<html>
	
	<head>	
		<meta charset="utf-8">
		<title>PHP - Project Blog</title>
		
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/pageElements.css">
		<link rel="stylesheet" href="css/debug.css">
	</head>
	
	<body>	
		<!-- -------- PAGE HEADER -------- -->
		<header class="fright loginheader">
			<p class="fright"><a href="?action=logout"><< Logout</a></p>	
			<br>
			<p class="fright"><a href="?action=zumIndex"><< Zum Frontend</a></p>

		</header>
		<div class="clearer"></div>
		
		<hr>
		<!-- -------- PAGE HEADER END -------- -->
		<h1>PHP - Project Blog - Dashboard</h1>
		<h3>Aktiver Benutzer: <?= $userFirstName ?> <?= $userLastName ?></h3>
		
		<!-- ---------- USER MESSAGES START ---------- -->
		<?php if($dbError): ?>
			<h3 class="error"><?= $dbError ?></h3>
		<?php elseif($dbSuccess): ?>
			<h3 class="success"><?= $dbSuccess ?></h3>
		<?php endif ?>
		<!-- ---------- USER MESSAGES END ---------- -->
		
		<!-- -------- PAGE CONTENT START -------- -->

<form action="<?php echo $_SERVER['SCRIPT_NAME'] ?>" method="POST" enctype="multipart/form-data">
<input type="hidden" name="newBlog">
		<p>Neue Kategorie anlegen</p>
		<form action="" method="POST" enctype="multipart/form-data">
			<input type="hidden" name="blogCategory">
				<fieldset>
					<input class="short" type="text" name="catLabel" placeholder="Name der Kategorie" >
					<span class="error" style="color: red"><?php echo $catlabelErrormessage ?></span><br>
					<input class="short" type="submit" value="Neue Kategorie anlegen">
				</fieldset>
		</form>
		<p>Neue Blog - Eintrag verfassen</p>
		<form action="" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="blogContent">
				
				<fieldset>
					
						<select name="catLabels">
						<?php foreach( $categoriesArray AS $category ): ?> {
							<option value='<?= $category['catID'] ?>'><?= $category['catLabel'] ?></option>\r\n";
						<?php endforeach ?>
					</select>
					<br>
					<br>
				
					
					<span class="error" style="color:red"><?php echo $errorblogHeading ?></span><br>
					<input class="short" type="text" placeholder="Überschrift" name="heading" value="<?php echo $blogHeading ?>">
				</fieldset>
				<p>Bild hochladen:</p>
				<!-- -------- IMAGE UPLOAD START--------  -->
				<fieldset name="avatar">
					<legend>Image</legend>
					<span class="error"><?php echo $errorImageUpload ?></span><br>	
					<input type="file" name="avatar">				
				<!-- -------- IMAGE UPLOAD END -------- -->
					
					
					<select name="blogImageAlignment">
						<option value="left">align left</option>
						<option value="right">align right</option>
					</select>
				</fieldset>				
				<!-- -------- IMAGE UPLOAD END -------- -->
				
				<fieldset name="accountdata">
				<legend>Text</legend>

				<span class="error"style="color:red"><?php echo $errorblogContent ?></span><br>
				<textarea name="blogDatas" placeholder="Blog Content..." ><?php echo $blogContent ?></textarea>
				</fieldset>	
				
				<input type="submit" value="Veröffentlichen">
				
			
			</form>
	</form>
		
		
		<!-- -------- PAGE CONTENT END --------- -->
		
		
		
		

		
		
		
		
		
		
	
		
		
		
		
		
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		

		
	</body>
	
</html>