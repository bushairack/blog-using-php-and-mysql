### A rudimentary blog system is to be programmed, which must fulfill the following tasks:

#### dashboard.php page:

* Creation of new categories (form – input text)
     * Before creating a new category, it must be checked whether the category name already exists in the DB.
* Creating a blog entry (form):
    * Assign a category (select box)
    * Writing a headline (input text)
    * Upload an image (input file)
    * Setting the alignment of the image (right/left of the text - select box)
    * Writing a text (Textarea)
    * Uploading an image must not be mandatory; it should also be possible to set texts without images.
* Logout of the author(s) (Link)

#### index.php page:
* Login for the blog author(s) on the secure editor page (form): The login should be via the email address of the user.
* Display of all existing blog entries, each of which should contain the following (standard when the page is called up, also via a link):
    * category
    * headline
    * Author, place, date, time
    * Blog text with picture if necessary
* View blog posts by category (via links)

