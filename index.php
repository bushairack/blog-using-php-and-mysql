<?php
#***************************************************************************************#

				#****************************************#
				#********** PAGE CONFIGURATION **********#
				#****************************************#


				require_once('./include/config.inc.php');
				require_once('./include/form.inc.php');
				require_once('./include/db.inc.php');


#***************************************************************************************#


				#***************************************#
				#******** INITIALIZE VARIABLES *********#
				#***************************************#
				
				$errorLogin 			= NULL;
				$erroruserEmail 		= NULL;
				$erroruserPassword 		= NULL;
				$selectedCategory  		= NULL;
				$showDashBoard			= false;
				$showLoginData 			= true;
				
#***************************************************************************************#

				#********************************************#
				#********** PROCESS URL PARAMETERS **********#
				#********************************************#
				
				#********** PREVIEW GET ARRAY **********#
/*
if(DEBUG_V)	echo "<pre class='debug value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)	print_r($_GET);					
if(DEBUG_V)	echo "</pre>";
*/

#***************************************************************************************#

				#****************************************#
				#********** PROCESS FORM LOGIN **********#
				#****************************************#
				
				#********** PREVIEW POST ARRAY **********#
/*
if(DEBUG_V)	echo "<pre class='debug value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)	print_r($_POST);					
if(DEBUG_V)	echo "</pre>";
*/
				#****************************************#
								
				// Schritt 1 FORM: Prüfen, ob Formular abgeschickt wurde
				if( isset($_POST['formLogin']) ) {
if(DEBUG)			echo "<p class='debug'>🧻 <b>Line " . __LINE__ . "</b>: Formular 'Login' wurde abgeschickt. <i>(" . basename(__FILE__) . ")</i></p>\n";										
				
						// Schritt 2 FORM: Werte auslesen, entschärfen, DEBUG-Ausgabe
if(DEBUG)			echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Werte auslesen und entschärfen... <i>(" . basename(__FILE__) . ")</i></p>\n";

						$userEmail 		= cleanString( $_POST['userEmail'] );
						$password 	= cleanString( $_POST['userPassword'] );
					
if(DEBUG_V)			echo "<p class='debug value'><b>Line " . __LINE__ . "</b>: \$userEmail: $userEmail <i>(" . basename(__FILE__) . ")</i></p>\n";
if(DEBUG_V)			echo "<p class='debug value'><b>Line " . __LINE__ . "</b>: \$userPassword: $password <i>(" . basename(__FILE__) . ")</i></p>\n";
				
						// Schritt 3 FORM: Feldvalidierung
if(DEBUG)			echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Feldwerte werden validiert... <i>(" . basename(__FILE__) . ")</i></p>\n";
					
						$errorUserEmail 	= validateEmail($userEmail, minLength:4, maxLength:20);
						$errorUserPassword = checkInputString($password, minLength:4);
if(DEBUG_V)			echo "<p class='debug value'><b>Line " . __LINE__ . "</b>: \$errorUserEmail: $errorUserEmail <i>(" . basename(__FILE__) . ")</i></p>\n";
if(DEBUG_V)			echo "<p class='debug value'><b>Line " . __LINE__ . "</b>: \$errorUserPassword: $errorUserPassword <i>(" . basename(__FILE__) . ")</i></p>\n";
					
									
						#********** FINAL FORM VALIDATION **********#
						if( $errorUserEmail !== NULL OR $errorUserPassword !== NULL ) {
							// Fehlerfall
if(DEBUG)				echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: Das Formular enthält noch Fehler! <i>(" . basename(__FILE__) . ")</i></p>\n";				
						
							// NEUTRALE Fehlermeldung für User ausgeben
							$errorLogin = 'Die Logindaten sind ungültig!';
						
						} else {
							// Erfolgsfall
if(DEBUG)				echo "<p class='debug ok'><b>Line " . __LINE__ . "</b>: Das Formular ist formal fehlerfrei. <i>(" . basename(__FILE__) . ")</i></p>\n";				
						
							// Schritt 4 FORM: Daten weiterverarbeiten
							$showDashBoard = true;
							$showLoginData = false;
						
							#********** VALIDATE LOGIN **********#	
						
							#**********************************#
							#********** DB OPERATION **********#
							#**********************************#
						
							// Schritt 1 DB: DB-Verbindung herstellen
							$PDO = dbConnect('blog');
						
						
							#********** FETCH ACCOUNT DATA FROM DATABSE BY USER EMAIL  **********#
if(DEBUG)				echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Lese Accountdaten zum empfangenen Accountnamen aus... <i>(" . basename(__FILE__) . ")</i></p>\n";
						
							$sql 		= 'SELECT userPassword, userID, userFirstName FROM users
										WHERE userEmail = :ph_userEmail';
						
							$params 	= array( 'ph_userEmail' => $userEmail );
						
							// Schritt 2 DB: SQL-Statement vorbereiten
							$PDOStatement = $PDO->prepare($sql);
						
							// Schritt 3 DB: SQL-Statement ausführen und ggf. Platzhalter füllen
							try {	
								$PDOStatement->execute($params);						
							} catch(PDOException $error) {
if(DEBUG)					echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: FEHLER: " . $error->GetMessage() . "<i>(" . basename(__FILE__) . ")</i></p>\n";										
								$dbError = 'Fehler beim Zugriff auf die Datenbank!';
							}

							// Schritt 4 DB: Daten weiterverarbeiten
							$row = $PDOStatement->fetch(PDO::FETCH_ASSOC);
							//$row = $PDOStatement->fetchAll(PDO::FETCH_ASSOC);

if(DEBUG_V)				echo "<pre class='debug value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)				print_r($row);					
if(DEBUG_V)				echo "</pre>";
#***************************************************************************************

							#********** 1. VALIDATE USER **********#
							if( $row === false ) {
								// Fehlerfall
if(DEBUG)					echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: Der UserEmail '$userEmail' existiert nicht in der Datenbank! <i>(" . basename(__FILE__) . ")</i></p>\n";				
							
								// NEUTRALE Fehlermeldung für User ausgeben
								$errorLogin = 'Die Logindaten sind ungültig!';
							
							} else {
								// Erfolgsfall
if(DEBUG)					echo "<p class='debug ok'><b>Line " . __LINE__ . "</b>: Der UserEmail '$userEmail' wurde in der Datenbank gefunden. <i>(" . basename(__FILE__) . ")</i></p>\n";				
							
								#********** 2. VALIDATE PASSWORD **********#
								if( $password !== $row['userPassword'] ) {
									// Fehlerfall
if(DEBUG)						echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: Das Passwort stimmt NICHT mit dem Passwort aus der Datenbank überein! <i>(" . basename(__FILE__) . ")</i></p>\n";				
								
									// NEUTRALE Fehlermeldung für User ausgeben
									$errorLogin = 'Die Logindaten sind ungültig!';
							
								} else {
									// Erfolgsfall
if(DEBUG)						echo "<p class='debug ok'><b>Line " . __LINE__ . "</b>: Das Passwort stimmt mit dem Passwort aus der Datenbank überein. <i>(" . basename(__FILE__) . ")</i></p>\n";				
															
										#********** 4. PROCESS LOGIN **********#
if(DEBUG)							echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Login wird durchgeführt... <i>(" . basename(__FILE__) . ")</i></p>\n";
									


									#********** PREPARE SESSION **********#		
									session_name('blog');

									#********** START SESSION **********#
									if( session_start() === false ) {
										// Fehlerfall
if(DEBUG)							echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: FEHLER beim Starten der Session! <i>(" . basename(__FILE__) . ")</i></p>\n";				
										
									} else {
									//session_regenerate_id(true);
if(DEBUG)							echo "<p class='debug ok'><b>Line " . __LINE__ . "</b>: Session erfolgreich gestartet. <i>(" . basename(__FILE__) . ")</i></p>\n";				

										$_SESSION['ip_address']	= $_SERVER['REMOTE_ADDR'];
										$_SESSION['userID'] 		= $row['userID'];	
										header('LOCATION: dashboard.php');
										//exit;
									}
		
								//$showDashBoard = true;									
								//$showLoginData = false;													

							}
						}
					}
				}
				
			#****************************************#
						// Schritt 1 URL: Prüfen, ob URL-Parameter übergeben wurde
						if( isset($_GET['action']) ) {
if(DEBUG)				echo "<p class='debug'>🧻 <b>Line " . __LINE__ . "</b>: URL-Parameter 'action' wurde übergeben. <i>(" . basename(__FILE__) . ")</i></p>\n";										
					
							// Schritt 2 URL: Werte auslesen, entschärfen, DEBUG-Ausgabe
							$action = cleanString($_GET['action']);
if(DEBUG_V)				echo "<p class='debug value'><b>Line " . __LINE__ . "</b>: \$action: $action <i>(" . basename(__FILE__) . ")</i></p>\n";
					
							// Schritt 3 URL: Verzweigung

							#********** DASH BOARD **********#
							if( $action === NULL ) {
								
								//header('LOCATION: index.php');
							$selectedCategory		= NULL;
						} else{
							$selectedCategory		= $action;
						} if( $action === 'index.php' ) {
							$showDashBoard = true;									
							$showLoginData = false;
						}
						
					}
						
#***************************************************************************************#

						#**********************************#
						#********** DB OPERATION **********#
						#**********************************#

						// Schritt 1 DB: DB-Verbindung herstellen
						$PDO = dbConnect('blog');

						#********** FETCH ACCOUNT DATA FROM BLOGS  **********#
if(DEBUG)			echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Lese Accountdaten zum empfangenen Accountnamen aus... <i>(" . basename(__FILE__) . ")</i></p>\n";
						
						$sql 		= 'SELECT * FROM blogs';
						
						$params 	= array( );
						
						// Schritt 2 DB: SQL-Statement vorbereiten
						$PDOStatement = $PDO->prepare($sql);
						
						// Schritt 3 DB: SQL-Statement ausführen und ggf. Platzhalter füllen
						try {	
							$PDOStatement->execute($params);						
						} catch(PDOException $error) {
if(DEBUG)				echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: FEHLER: " . $error->GetMessage() . "<i>(" . basename(__FILE__) . ")</i></p>\n";										
							$dbError = 'Fehler beim Zugriff auf die Datenbank!';
						}
						
						$blogsArray	= $PDOStatement->fetchAll(PDO::FETCH_ASSOC);
						
						// DB-Verbindung beenden
						unset($PDO);
/*						
if(DEBUG_V)	echo "<pre class='debug value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)	print_r($blogsArray);					
if(DEBUG_V)	echo "</pre>";
*/


						// Schritt 1 DB: DB-Verbindung herstellen
						$PDO = dbConnect('blog');

						#********** FETCH ACCOUNT DATA FROM BLOGS  **********#
if(DEBUG)			echo "<p class='debug'>📑 <b>Line " . __LINE__ . "</b>: Lese Accountdaten zum empfangenen Accountnamen aus... <i>(" . basename(__FILE__) . ")</i></p>\n";
						
						$sql 		= 'SELECT * FROM categories';
						
						$params 	= array( );
						
						// Schritt 2 DB: SQL-Statement vorbereiten
						$PDOStatement = $PDO->prepare($sql);
						
						// Schritt 3 DB: SQL-Statement ausführen und ggf. Platzhalter füllen
						try {	
							$PDOStatement->execute($params);						
						} catch(PDOException $error) {
if(DEBUG)				echo "<p class='debug err'><b>Line " . __LINE__ . "</b>: FEHLER: " . $error->GetMessage() . "<i>(" . basename(__FILE__) . ")</i></p>\n";										
							$dbError = 'Fehler beim Zugriff auf die Datenbank!';
						}
						
						$categoriesArray	= $PDOStatement->fetchAll(PDO::FETCH_ASSOC);
						
						// DB-Verbindung beenden
						unset($PDO);

if(DEBUG_V)	echo "<pre class='debug value'>Line <b>" . __LINE__ . "</b> <i>(" . basename(__FILE__) . ")</i>:<br>\n";					
if(DEBUG_V)	print_r($categoriesArray);					
if(DEBUG_V)	echo "</pre>";						





#***************************************************************************************#
?>


<!doctype html>

<html>
	
	<head>	
		<meta charset="utf-8">
		<title>PHP-Projekt Blog</title>
		
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/pageElements.css">
		<link rel="stylesheet" href="css/debug.css">
	</head>
	
	<body>	
		
		<!--	<?php// if( $showLoginData === true ): ?>	-->
		<!-- -------- LOGIN FORM START -------- -->
			<form action="<?php echo $_SERVER['SCRIPT_NAME'] ?>" method="POST">
				<input type="hidden" name="formLogin">
				<fieldset>
					<legend>Login</legend>					
					<span class='error' style="color: red"><?php echo $errorLogin ?></span><br>
					<input class="short" type="text" name="userEmail" placeholder="Email">
					<input class="short" type="password" name="userPassword" placeholder="Password">
					<input class="short" type="submit" value="Login">
				</fieldset>
			</form>
		<!-- -------- LOGIN FORM END -------- -->
		<!--	<?php// endif ?>	-->
		
			<?php if( $showDashBoard === true ): ?>	
				<!-- -------- PAGE HEADER -------- -->
		
		<header class="fright loginheader">
			<p class="fright"><a href="?action=logout"><< Logout</a></p>
			<br>			
			<p class="fright"><a href="?action=dashboard">Zum Dash board >></a></p>	
		</header>
		<div class="clearer"></div>
		
		<hr>
		
		<!-- -------- PAGE HEADER END -------- -->
		
		<?php endif ?>	

		
		<h1>PHP-Projekt Blog</h1>
		<h5>Alle Einträge anzeigen</h5>
		
		<?php if( $selectedCategory === NULL ) :?>	
		
	<div class="row">
		<div class="column" style="width:50%">
			<!-- <p style="text-align:right; margin-right:2em">Kategorie: </p> -->
		<?php foreach( $blogsArray AS $blog ): ?>
		<?php foreach( $categoriesArray AS $category ): ?>
		
		
		<?php if($blog['catID'] === $category['catID']) :?>
		<p style="text-align:right; margin-right:2em">Kategorie: <?php echo $category['catLabel']?></p>
		<?php endif ?>
		<?php endforeach ?>
		<!--	<p><?php echo $blog['blogID'] ?></p>	-->
			<h2 style="color:darkblue; margin-left:2em;"><?php echo $blog['blogHeadline'] ?></h2>
			<p style="color:gray; margin-left:2em;"><?php echo $blog['blogDate'] ?></p>
		<!--	<p><?php echo $blog['blogImagePath'] ?></p>	-->
			<?php  if( $blog['blogImagePath'] !== NULL):?>
			<img class="avatar" src="<?php echo $blog['blogImagePath'] ?>" alt="Image von <?php echo $userFirstName ?>" title="Image von <?php echo $userFirstName ?>" style="float:<?php echo $blog['blogImageAlignment'] ?>"><br><br></p>
			<?php endif ?>
		<!--	<p><?php echo $blog['blogImageAlignment'] ?></p>	-->
			<p style="color:brown; margin-left:2em;"><?php echo $blog['blogContent'] ?></p><br>
			
		<br>
		<br>
		<br>
		<br>
		<br>
		
		<?php endforeach ?>
		<br>
		<?php endif ?>
		</div>
		</form>
			<div class="column" style="width:50%">
			<?php if( $selectedCategory !== NULL ) :?>
			<?php foreach( $categoriesArray AS $category ): ?>
			<?php foreach( $blogsArray AS $blog ): ?>
			<?php if($selectedCategory === $category['catLabel']) :?>
			<p style="text-align:right; margin-right:2em"></p>
			<?php if($blog['catID'] === $category['catID']) :?> 
			<h2 style="color:darkblue; margin-left:2em;"><?php echo $blog['blogHeadline'] ?></h2>
			<p style="color:gray; margin-left:2em;"><?php echo $blog['blogDate'] ?></p>
			<?php  if( $blog['blogImagePath'] !== NULL):?>
			<img class="avatar" src="<?php echo $blog['blogImagePath'] ?>" alt="Image von <?php echo $userFirstName ?>" title="Image von <?php echo $userFirstName ?>" style="float:<?php echo $blog['blogImageAlignment'] ?>"><br><br></p>
			<?php endif ?>
			<p style="color:brown; margin-left:2em;"><?php echo $blog['blogContent'] ?></p>
			<br>
			<br>
			<br>
			
			<?php endif ?>
			<?php endif ?>
			<?php endforeach ?>
			<?php endforeach ?>
			<?php endif ?>
			<br>
			<br>
			<br>
			</div>
		<form action="<?php echo $_SERVER['SCRIPT_NAME'] ?>" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="categoriesDisplay">
		<div class="column">
		<?php foreach( $categoriesArray AS $category ): ?>
			<h2 style="text-align:left; width:50%"><a style="color:orange; text-align:left" href="?action=<?php echo $category['catLabel'] ?>"><?php echo $category['catLabel'] ?></h2><br>
		<?php endforeach ?>
		</div>
	</div>
		
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		
	</body>
	
</html>